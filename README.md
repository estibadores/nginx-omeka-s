# NGINX para Omeka-S

Imagen de _NGINX_ que hace de proxy para un _PHP-FPM_

Deben compartir el mismo volumen para poder usar la misma instalación de _Omeka S_

El _root_ de ficheros se encuentra en el el volumen _/sindominio/omeka-s_

Se puede modificar el config en _sd.conf_

Y los parametros del proxy php en el fichero de parametros _fastcgi_params_
