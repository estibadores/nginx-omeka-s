FROM registry.sindominio.net/debian

RUN apt-get update && \
    apt-get -qy install nginx-full &&\
    apt-get clean

COPY sd.conf /etc/nginx/sites-available/sd.conf
COPY fastcgi_params /etc/nginx/fastcgi_params
RUN rm /etc/nginx/sites-enabled/default
RUN ln -s /etc/nginx/sites-available/sd.conf /etc/nginx/sites-enabled/sd.conf

VOLUME /sindominio/

EXPOSE 80

CMD /usr/sbin/nginx -g "daemon off; master_process off;"
